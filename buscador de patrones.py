import sqlite3, csv, sys, json

patrons = []

to_analize = []

with open("datos/transit_relacio_trams.csv") as File:
    reader = csv.DictReader(File)

    for row in reader:
        cod = row["Descripcio"]

        cod = cod.replace("Ã¨", "è")
        cod = cod.replace("Ã©", "é")
        cod = cod.replace("Ã­", "í")
        cod = cod.replace("Ã ", "à")
        cod = cod.replace("Ã³", "ó")
        cod = cod.replace("Ã²", "ò")
        cod = cod.replace("Ã±", "ñ")
        cod = cod.replace("Ãº", "ú")
        cod = cod.replace("Ã¯", "ï")
        cod = cod.replace("Ã¯", "ü")
        cod = cod.replace("Ã§", "ç")

        to_analize.append(cod)

base = sqlite3.connect("datos.db")

cursor = base.cursor()

data = cursor.execute("SELECT * FROM denso WHERE Estado=?", ("Congestionado",))

data = data.fetchall()

total = len(to_analize) * 24

i = 0

for dat in data:
    name = dat[0]

    if name in to_analize:
        day = str(dat[1]) + "/" + str(dat[2]) + "/" + str(dat[3])

        for hour in range(0, 24):
            i += 1

            print(str(i) + "/" + str(total))

            data_2 = cursor.execute("SELECT * FROM denso WHERE Estado=? and Calle=? and Hora=?", ("Congestionado", name, hour))

            data_2 = cursor.fetchall()

            if len(data_2) > 10:
                patrons.append(data_2[0])

        print(name)

        to_analize.remove(name)


    if len(to_analize) == 0:
        break

json.dump(patrons, open("patrons.json", "w"))

# pprint.pprint(data)