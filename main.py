# -*- coding: utf-8 -*-

import csv
import json
import sqlite3
import datetime
import time


def write_data(data):
    print("Writing")

    File = open('datos.csv', 'a')
    with File:
        writer = csv.writer(File)
        writer.writerows(data)

    print("Writed")

    time.sleep(5)


codigos = {}

File = open('datos.csv', 'w')
with File:
    writer = csv.writer(File)
    writer.writerows([["Calle", "Dia", "Mes", "Año", "Hora", "Minuto", "Segundo", "Estado"],])

# Leemos el csv de los codigos de calles
with open("datos/transit_relacio_trams.csv") as File:
    reader = csv.DictReader(File)

    for row in reader:
        cod = row["Descripcio"]

        cod = cod.replace("Ã¨", "è")
        cod = cod.replace("Ã©", "é")
        cod = cod.replace("Ã­", "í")
        cod = cod.replace("Ã ", "à")
        cod = cod.replace("Ã³", "ó")
        cod = cod.replace("Ã²", "ò")
        cod = cod.replace("Ã±", "ñ")
        cod = cod.replace("Ãº", "ú")
        cod = cod.replace("Ã¯", "ï")
        cod = cod.replace("Ã¯", "ü")
        cod = cod.replace("Ã§", "ç")

        codigos[row["Tram"]] = cod

datos = []

files = ["2017_10_Octubre_TRAMS_TRAMS.csv",
         "2017_11_Novembre_TRAMS_TRAMS.csv",
         "2017_12_Desembre_TRAMS_TRAMS.csv",
         "2018_01_Gener_TRAMS_TRAMS.csv",
         "2018_02_Febrer_TRAMS_TRAMS.csv",
         "2018_03_Marc_TRAMS_TRAMS.csv",
         "2018_04_Abril_TRAMS_TRAMS.csv",
         "2018_05_Maig_TRAMS_TRAMS.csv",
         "2018_06_Juny_TRAMS_TRAMS.csv",
         "2018_07_Juliol_TRAMS_TRAMS.csv",
         "2018_08_Agost_TRAMS_TRAMS.csv",
         "2018_09_Setembre_TRAMS_TRAMS.csv",
         "2018_10_Octubre_TRAMS_TRAMS.csv",
         "2018_11_Novembre_TRAMS_TRAMS.csv",
         "2018_12_Desembre_TRAMS_TRAMS.csv",
         "2019_01_Gener_TRAMS_TRAMS.csv"]

back_day = 1

for file_name in files:
    with open("datos/" + file_name) as File:
        reader = csv.DictReader(File)

        for row in reader:
            try:
                if row["data"][6:8] != back_day:
                    write_data(datos)
                    datos = []
                    back_day = row["data"][6:8]

                num = int(row["estatActual"])

                if num == 0:
                    dat = [
                        codigos[row["idTram"]],
                        row["data"][6:8],
                        row["data"][4:6],
                        row["data"][:4],
                        row["data"][8:10],
                        row["data"][10:12],
                        row["data"][12:14],
                        "Sin datos"
                    ]

                elif num == 1:
                    dat = [
                        codigos[row["idTram"]],
                        row["data"][6:8],
                        row["data"][4:6],
                        row["data"][:4],
                        row["data"][8:10],
                        row["data"][10:12],
                        row["data"][12:14],
                        "Muy fluido"
                    ]

                elif num == 2:
                    dat = [
                        codigos[row["idTram"]],
                        row["data"][6:8],
                        row["data"][4:6],
                        row["data"][:4],
                        row["data"][8:10],
                        row["data"][10:12],
                        row["data"][12:14],
                        "Fluido"
                    ]

                elif num == 3:
                    dat = [
                        codigos[row["idTram"]],
                        row["data"][6:8],
                        row["data"][4:6],
                        row["data"][:4],
                        row["data"][8:10],
                        row["data"][10:12],
                        row["data"][12:14],
                        "Denso"
                    ]

                elif num == 4:
                    dat = [
                        codigos[row["idTram"]],
                        row["data"][6:8],
                        row["data"][4:6],
                        row["data"][:4],
                        row["data"][8:10],
                        row["data"][10:12],
                        row["data"][12:14],
                        "Muy denso"
                    ]

                elif num == 5:
                    dat = [
                        codigos[row["idTram"]],
                        row["data"][6:8],
                        row["data"][4:6],
                        row["data"][:4],
                        row["data"][8:10],
                        row["data"][10:12],
                        row["data"][12:14],
                        "Congestionado"
                    ]

                else:
                    dat = [
                        codigos[row["idTram"]],
                        row["data"][6:8],
                        row["data"][4:6],
                        row["data"][:4],
                        row["data"][8:10],
                        row["data"][10:12],
                        row["data"][12:14],
                        "Cortado"
                    ]

                datos.append(dat)

                print(dat)
            except KeyError:
                pass


