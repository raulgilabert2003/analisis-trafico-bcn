import sqlite3, csv
import matplotlib.pyplot as plt
import random

calles = []

with open("datos/transit_relacio_trams.csv") as File:
    reader = csv.DictReader(File)

    for row in reader:
        cod = row["Descripcio"]

        cod = cod.replace("Ã¨", "è")
        cod = cod.replace("Ã©", "é")
        cod = cod.replace("Ã­", "í")
        cod = cod.replace("Ã ", "à")
        cod = cod.replace("Ã³", "ó")
        cod = cod.replace("Ã²", "ò")
        cod = cod.replace("Ã±", "ñ")
        cod = cod.replace("Ãº", "ú")
        cod = cod.replace("Ã¯", "ï")
        cod = cod.replace("Ã¯", "ü")
        cod = cod.replace("Ã§", "ç")

        calles.append(cod)

num = random.randint(1, len(calles) - 10)

calle = calles[num]

base = sqlite3.connect("datos.db")

cursor = base.cursor()

cursor.execute("SELECT * FROM datos WHERE Calle=? and Mes=? and Estado=?", (calle, 11, "Cortado"))

Cortado = cursor.fetchall()

cursor.execute("SELECT * FROM datos WHERE Calle=? and Mes=? and Estado=?", (calle, 11, "Sin datos"))

Sin_datos = cursor.fetchall()

cursor.execute("SELECT * FROM datos WHERE Calle=? and Mes=? and Estado=?", (calle, 11, "Muy fluido"))

Muy_fluido = cursor.fetchall()

cursor.execute("SELECT * FROM datos WHERE Calle=? and Mes=? and Estado=?", (calle, 11, "Fluido"))

Fluido = cursor.fetchall()

cursor.execute("SELECT * FROM datos WHERE Calle=? and Mes=? and Estado=?", (calle, 11, "Denso"))

Denso = cursor.fetchall()

cursor.execute("SELECT * FROM datos WHERE Calle=? and Mes=? and Estado=?", (calle, 11, "Muy denso"))

Muy_denso = cursor.fetchall()

cursor.execute("SELECT * FROM datos WHERE Calle=? and Mes=? and Estado=?", (calle, 11, "Congestionado"))

Congestionado = cursor.fetchall()

denso = len(Denso)
muy_denso = len(Muy_denso)
congestionado = len(Congestionado)
cortado = len(Cortado)
sin_datos = len(Sin_datos)
fluido = len(Fluido)
muy_fluido = len(Muy_fluido)

fig = plt.figure(u"Gráfico de barras de la calle " + calle)

fig.set_size_inches(15, 8)

datos = [sin_datos, muy_fluido, fluido, denso, muy_denso, congestionado, cortado]

xx = range(len(datos))

ax = fig.add_subplot(111)
ax.bar(xx, datos, width=0.8, align="center")
ax.set_xticks(xx)
           
ax.set_xticklabels(["Sin datos", "Muy fluido", "Fluido", "Denso", "Muy denso", "Congestionado", "Cortado"])

plt.show()
